package net.jasonstone.mailthing;

import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class MailThing extends JavaPlugin implements Listener {
	public Logger logger;
	public DBStuff db;

	@Override
	public void onEnable() {
		logger = getLogger();

		getServer().getPluginManager().registerEvents(this, this);

		db = new DBStuff(this);
		logger.info(db.toString());

		PluginCommand mailCommand = getCommand("shipit");
		MailCommand mailExecutor = new MailCommand(this);
		mailCommand.setExecutor(mailExecutor);
		mailCommand.setTabCompleter(mailExecutor);

		PluginCommand createCommand = getCommand("createmailbox");
		CreateCommand createExecutor = new CreateCommand(this);
		createCommand.setExecutor(createExecutor);

		PluginCommand deleteCommand = getCommand("deletemailbox");
		DeleteCommand deleteExecutor = new DeleteCommand(this);
		deleteCommand.setExecutor(deleteExecutor);

		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new MailTask(this), 0, 50);
	}
}

class MailTask implements Runnable {

	private final MailThing plugin;

	public MailTask(MailThing plugin) {
		this.plugin = plugin;
	}

	public void run() {
		// plugin.logger.info("waaaat");

		ArrayList<Mailbox> boxes = plugin.db.getMailboxes();

		for (Mailbox box : boxes) {
			box.ding();
		}
	}

}
