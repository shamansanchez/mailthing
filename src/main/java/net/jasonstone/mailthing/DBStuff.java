package net.jasonstone.mailthing;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.bukkit.block.Block;

public class DBStuff {

  private MailThing plugin;

  public DBStuff(MailThing plugin) {
    this.plugin = plugin;

    try (Connection connection = _getConnection()) {
      String query = "CREATE TABLE IF NOT EXISTS mailboxes(name STRING PRIMARY KEY, world STRING NOT NULL, x INTEGER NOT NULL, y INTEGER NOT NULL, z INTEGER NOT NULL, UNIQUE(world,x,y,z));";
      Statement statement = connection.createStatement();
      statement.executeUpdate(query);
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private Connection _getConnection() throws SQLException {
    Connection connection = DriverManager
        .getConnection("jdbc:sqlite:" + plugin.getDataFolder() + File.separator + "mailthing.db");
    return connection;
  }

  public Mailbox addMailbox(String name, String world, int x, int y, int z) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.prepareStatement("INSERT INTO mailboxes VALUES(?, ?, ?, ?, ?)");
      statement.setString(1, name);
      statement.setString(2, world);

      statement.setInt(3, x);
      statement.setInt(4, y);
      statement.setInt(5, z);

      statement.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }

    return new Mailbox(name, world, x, y, z, true);
  }

  public boolean deleteMailbox(String name) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {

      statement = connection.prepareStatement("DELETE FROM mailboxes WHERE name=?");
      statement.setString(1, name);

      statement.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    }

    return true;
  }

  public ArrayList<Mailbox> getMailboxes() {
    ArrayList<Mailbox> ret = new ArrayList<Mailbox>();

    Statement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.createStatement();
      ResultSet result = statement.executeQuery("SELECT * FROM mailboxes");
      while (result.next()) {
        ret.add(new Mailbox(result.getString("name"), result.getString("world"), result.getInt("x"), result.getInt("y"),
            result.getInt("z"), false));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }

    return ret;
  }

  public ArrayList<String> getMailboxNames() {
    ArrayList<String> ret = new ArrayList<String>();

    Statement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.createStatement();
      ResultSet result = statement.executeQuery("SELECT name FROM mailboxes");
      while (result.next()) {
        ret.add(result.getString("name"));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }

    return ret;
  }

  public Mailbox getMailbox(String name) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.prepareStatement("SELECT * FROM mailboxes WHERE name=?");
      statement.setString(1, name);

      ResultSet result = statement.executeQuery();
      if (!result.next()) {
        return null;
      }

      return new Mailbox(result.getString("name"), result.getString("world"), result.getInt("x"), result.getInt("y"),
          result.getInt("z"), true);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public Mailbox getMailboxByBlock(Block block) {
    PreparedStatement statement;
    try (Connection connection = _getConnection()) {
      statement = connection.prepareStatement("SELECT * FROM mailboxes WHERE world=? AND x=? AND y=? AND z=?");
      statement.setString(1, block.getWorld().getName());
      statement.setInt(2, block.getX());
      statement.setInt(3, block.getY());
      statement.setInt(4, block.getZ());

      ResultSet result = statement.executeQuery();
      if (!result.next()) {
        return null;
      }

      return new Mailbox(result.getString("name"), result.getString("world"), result.getInt("x"), result.getInt("y"),
          result.getInt("z"), true);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

}
