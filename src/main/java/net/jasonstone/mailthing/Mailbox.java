package net.jasonstone.mailthing;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Barrel;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public class Mailbox {
  public Location location;
  public Block boxBlock;
  public Barrel barrel;

  public boolean exists;

  public String name;

  public ItemStack[] items;

  public World world;

  public String toString() {
    return String.format("Mailbox \"%s\": X:%d Y:%d Z:%d", name, boxBlock.getX(), boxBlock.getY(), boxBlock.getZ());
  }

  public Mailbox(String name, String worldName, int x, int y, int z, boolean init) {
    world = Bukkit.getWorld(worldName);
    location = new Location(world, x, y, z);

    this.name = name;
    boxBlock = location.getBlock();
    if (init) {
      exists = initMailbox();
    } else {
      exists = false;
    }
  }

  public boolean initMailbox() {
    try {
      barrel = (Barrel) boxBlock.getState();
      return true;
    } catch (ClassCastException e) {
      return false;
    }
  }

  public boolean isLoaded() {
    return world.isChunkLoaded(location.getBlockX() >> 4, location.getBlockZ() >> 4);
  }

  public boolean hasStuff() {
    if (!exists) {
      return false;
    }

    for (ItemStack item : barrel.getInventory().getContents()) {
      if (item != null) {
        return true;
      }
    }
    return false;
  }

  public ItemStack[] getContents() {
    if (!exists) {
      return new ItemStack[0];
    }

    ArrayList<ItemStack> list = new ArrayList<ItemStack>();
    for (ItemStack item : barrel.getInventory().getContents()) {
      if (item != null) {
        list.add(item);
      }
    }

    ItemStack[] ret = new ItemStack[list.size()];
    list.toArray(ret);

    return ret;
  }

  public HashMap<Integer, ItemStack> addContents(ItemStack... items) {
    if (!exists) {
      return new HashMap<Integer, ItemStack>();
    }

    return barrel.getInventory().addItem(items);
  }

  public void portalEffect() {
    world.spawnParticle(Particle.PORTAL, location.add(0.5f, 1, 0.5f), 100);
    world.playSound(location, Sound.BLOCK_END_PORTAL_FRAME_FILL, 1, 0.5f);
  }

  public void createEffect() {
    world.spawnParticle(Particle.CRIT_MAGIC, location.add(0.5f, 1, 0.5f), 100);
    world.playSound(location, Sound.BLOCK_BEACON_ACTIVATE, 1, 1f);
  }

  public void deleteEffect() {
    world.spawnParticle(Particle.CRIT_MAGIC, location.add(0.5f, 1, 0.5f), 50);
    world.spawnParticle(Particle.CRIT, location.add(0.5f, 1, 0.5f), 50);
    world.playSound(location, Sound.BLOCK_BEACON_DEACTIVATE, 1, 1f);
  }

  public void ding() {
    if (isLoaded()) {
      exists = initMailbox();
    }

    if (hasStuff()) {
      world.spawnParticle(Particle.SPELL_INSTANT, location.add(0.5f, 1, 0.5f), 50);
      return;
    }

    if (!exists) {
      world.spawnParticle(Particle.FIREWORKS_SPARK, location.add(0.5f, 1, 0.5f), 50);
      return;
    }

    world.spawnParticle(Particle.SPELL_WITCH, location.add(0.5f, 1, 0.5f), 5);
  }
}
