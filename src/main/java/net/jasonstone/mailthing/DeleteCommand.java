package net.jasonstone.mailthing;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DeleteCommand implements CommandExecutor {
  private final MailThing plugin;

  public DeleteCommand(MailThing t) {
    plugin = t;
  }

  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (!(sender instanceof Player)) {
      return false;
    }

    Player player = (Player) sender;
    Block block = player.getTargetBlockExact(10);
    if (block == null) {
      return false;
    }

    Mailbox deadBox = plugin.db.getMailboxByBlock(block);

    if (deadBox == null) {
      player.sendMessage(ChatColor.RED + "That's not a mailbox. Good job.");
      return true;
    }

    deadBox.deleteEffect();

    plugin.db.deleteMailbox(deadBox.name);

    return true;
  }
}
