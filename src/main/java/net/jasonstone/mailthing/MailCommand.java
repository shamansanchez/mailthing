package net.jasonstone.mailthing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.ImmutableList;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.StringUtil;

public class MailCommand implements CommandExecutor, TabCompleter {
	private final MailThing plugin;

	public MailCommand(MailThing t) {
		plugin = t;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		if (args.length != 1) {
			return false;
		}

		Player player = (Player) sender;
		Block block = player.getTargetBlockExact(10);
		if (block == null) {
			return false;
		}

		Mailbox sourceMailbox = plugin.db.getMailboxByBlock(block);

		if (sourceMailbox == null) {
			player.sendMessage(ChatColor.RED + "That isn't a mailbox, what are you doing?");
			return true;
		}

		Mailbox destMailbox = plugin.db.getMailbox(args[0]);
		if (destMailbox == null) {
			player.sendMessage(String.format(ChatColor.RED + "Unknown mailbox: \"%s\"", args[0]));
			return true;
		}

		if (sourceMailbox.name.equals(destMailbox.name)) {
			player.sendMessage(ChatColor.RED + "You can't send mail to yourself...");
			return true;
		}

		plugin.logger.info(String.format("Shipping from %s -> %s", sourceMailbox.name, destMailbox.name));

		if (!sourceMailbox.exists) {
			player.sendMessage(ChatColor.RED + "How did you send from a mailbox that doesn't exist?");
			return true;
		}

		ItemStack[] sourceStuff = sourceMailbox.getContents();

		if (!destMailbox.exists) {
			player.sendMessage(ChatColor.RED + "The destination mailbox is MIA.");
			return true;
		}

		sourceMailbox.barrel.getInventory().clear();
		sourceMailbox.portalEffect();
		HashMap<Integer, ItemStack> leftovers = destMailbox.addContents(sourceStuff);

		for (ItemStack item : leftovers.values()) {
			sourceMailbox.addContents(item);
		}

		if (leftovers.size() > 0) {
			player.sendMessage("The destination mailbox was full, so not all items were sent.");
		}
		destMailbox.portalEffect();

		return true;
	}

	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (args.length != 1) {
			return ImmutableList.of();
		}

		ArrayList<String> boxNames = plugin.db.getMailboxNames();
		return StringUtil.copyPartialMatches(args[0], boxNames, new ArrayList<String>(boxNames.size()));
	}
}
